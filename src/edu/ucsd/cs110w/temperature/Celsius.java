package edu.ucsd.cs110w.temperature;
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	} 
	public String toString()
	{
	// TODO: Complete this method return "";
		return Float.toString(this.getValue());
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float v = getValue();
		Celsius temp = new  Celsius(v); 
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float v = getValue();
		v = v * 9/5 + 32;
		Celsius temp = new  Celsius(v); 
		return temp;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return null;
	}
}