package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature
{

	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return String.valueOf(this.getValue());
		
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float convC = (5*(getValue()-32))/9;
		Fahrenheit convert = new Fahrenheit(convC);
		return convert;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float convF = getValue();
		Fahrenheit convert = new Fahrenheit(convF);
		return convert;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return null;
	}
}
